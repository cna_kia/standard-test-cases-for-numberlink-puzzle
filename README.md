# Standard Test Cases for Numberlink Puzzle

These test cases are standard test cases for the Numberlink puzzle. They have been tested over 50 times by the modified MCTS algorithm in the below link to be checked that are solvable. 
https://ieeexplore.ieee.org/abstract/document/8848043